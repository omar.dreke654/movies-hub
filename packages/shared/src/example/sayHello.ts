export const sayHello = (name = 'World'): string => {
  return `Hello, ${name}`;
};
