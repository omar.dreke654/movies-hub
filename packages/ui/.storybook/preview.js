import React from "react";
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import '@storybook/addon-console';

import { GlobalStyles } from "../src/particules/theme/GlobalStyles";

const GlobalWrapper = (storyFn, context) => {
  return (
    <div>
      <GlobalStyles />
      {storyFn()}
    </div>
  );
};

export const decorators = [GlobalWrapper];

export const parameters = {
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
  backgrounds: {
    default: 'white',
    values: [
      {
        name: 'black',
        value: 'rgb(40,40,40)'
      },
      {
        name: 'white',
        value: '#e5e5e5'
      }
    ]
  },
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: { hideNoControlsWarning: true },
};

export const globalTypes = {
  theme: {
    name: 'Theme',
    description: 'Global theme for components',
    defaultValue: 'light',
    toolbar: {
      icon: 'circlehollow',
      // array of plain string values or MenuItem shape (see below)
      items: ['light', 'dark'],
    },
  },
};
