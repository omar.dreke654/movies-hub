// eslint-disable-next-line @typescript-eslint/ban-types
export type MergeProps<M extends {}, C extends {}> = M & Omit<C, keyof M>;
