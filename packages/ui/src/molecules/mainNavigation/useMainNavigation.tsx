import { SerializedStyles } from '@emotion/core';
import React, { createContext, useCallback, useContext, useMemo } from 'react';
import * as styles from './mainNavigation.styles';

interface MainNavigationContextValue {
  currentPath: string;
  getLinkProps(
    path: string,
  ): {
    className?: string;
    css: SerializedStyles[] | SerializedStyles;
  };
}

const MainNavigationContext = createContext<MainNavigationContextValue | null>(null);
MainNavigationContext.displayName = 'MainNavigationContext';

type MainNavigationProviderProps = React.PropsWithChildren<{ currentPath: string }>;
export const MainNavigationProvider = ({
  currentPath,
  children,
}: MainNavigationProviderProps): React.ReactElement => {
  const getLinkProps = useCallback(
    (path: string): ReturnType<MainNavigationContextValue['getLinkProps']> => ({
      className: currentPath.startsWith(path) ? 'current' : undefined,
      css: [styles.link],
    }),
    [currentPath],
  );

  const memoizedValue = useMemo(() => ({ currentPath, getLinkProps }), [currentPath, getLinkProps]);

  return (
    <MainNavigationContext.Provider value={memoizedValue}>
      {children}
    </MainNavigationContext.Provider>
  );
};

export const useMainNavigation = (): MainNavigationContextValue => {
  const context = useContext(MainNavigationContext);

  if (!context) {
    throw new Error('useMainNavigation must be used within a <MainNavigationProvider />');
  }

  return context;
};
