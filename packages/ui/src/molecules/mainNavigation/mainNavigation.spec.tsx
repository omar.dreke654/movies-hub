import React, { ReactElement } from 'react';
import { fireEvent, render } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { MainNavigation } from './MainNavigation';
import { useMainNavigation } from './useMainNavigation';

const renderMainNavigation = (currentPath?: string): ReactElement => (
  <MainNavigation currentPath={currentPath}>
    <MainNavigation.Link href="/browse">Home</MainNavigation.Link>
    <MainNavigation.Link href="/movies">Movies</MainNavigation.Link>
    <MainNavigation.Link href="/tv-shows">TV Shows</MainNavigation.Link>
    <MainNavigation.Link href="/watchlist">Watchlist</MainNavigation.Link>
    <MainNavigation.Link href="/genres">Genres</MainNavigation.Link>
  </MainNavigation>
);

describe('molecules > MainNavigation', () => {
  it('snapshots - without currentPath', () => {
    const { asFragment } = render(renderMainNavigation());

    expect(asFragment()).toMatchSnapshot();
  });

  it('snapshots - with currentPath', () => {
    const { asFragment } = render(renderMainNavigation('/browse/123-123'));

    expect(asFragment()).toMatchSnapshot();
  });

  it('When menu is close', async () => {
    const { getByTestId } = render(renderMainNavigation());

    expect(getByTestId('main-navigation-list')).not.toBeVisible();
  });

  it('When currentPath is null', () => {
    const { getAllByRole, getByTestId } = render(renderMainNavigation());

    fireEvent.click(getByTestId('main-navigation-menu'));

    getAllByRole('link').forEach((el) => {
      expect(el).not.toHaveClass('current');
    });
  });

  it('When currentPath is "/browse/123-123"', () => {
    const { getByRole, getAllByRole, getByTestId } = render(
      renderMainNavigation('/browse/123-123'),
    );

    fireEvent.click(getByTestId('main-navigation-menu'));

    expect(getByRole('link', { name: /Home/ })).toHaveClass('current');

    getAllByRole('link', { name: /^(?!.*Home).*/ }).forEach((el) => {
      expect(el).not.toHaveClass('current');
    });
  });

  it("When use 'useMainNavigation' without 'MainNavigationProvider'", () => {
    const { result } = renderHook(() => useMainNavigation());

    expect(() => result.current).toThrowError(
      'useMainNavigation must be used within a <MainNavigationProvider />',
    );
  });
});
