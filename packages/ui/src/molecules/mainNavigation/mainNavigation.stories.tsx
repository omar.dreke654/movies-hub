import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { MainNavigation, MainNavigationProps } from './MainNavigation';

export default {
  title: 'Molecules/MainNavigation',
  component: MainNavigation,
} as Meta;

const Template: Story<MainNavigationProps> = (args) => (
  <div style={{ minHeight: '5rem', backgroundColor: '#000' }}>
    <MainNavigation {...args}>
      <MainNavigation.Link href="/home">Home</MainNavigation.Link>
      <MainNavigation.Link href="/browse">Browse</MainNavigation.Link>
      <MainNavigation.Link href="/movies">Movies</MainNavigation.Link>
      <MainNavigation.Link href="/tv-shows">TV Shows</MainNavigation.Link>
      <MainNavigation.Link href="/watchlist">Watchlist</MainNavigation.Link>
      <MainNavigation.Link href="/genres">Genres</MainNavigation.Link>
    </MainNavigation>
  </div>
);

export const Default = Template.bind({});
Default.args = {
  currentPath: '/browse/123-1223/show',
};
