import React from 'react';
import { render } from '@testing-library/react';
import { HeaderBrand } from './HeaderBrand';

describe('molecules > HeaderBrand', () => {
  it('snapshot', () => {
    const { asFragment } = render(<HeaderBrand />);

    expect(asFragment()).toMatchSnapshot();
  });
});
