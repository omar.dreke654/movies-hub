import { css } from '@emotion/core';

export const container = css`
  position: relative;
  width: 100%;
  height: calc(100vw * 9 / 16);
`;
