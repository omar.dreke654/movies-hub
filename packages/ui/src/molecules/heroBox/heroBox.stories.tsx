import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { HeroBox, HeroBoxProps } from './HeroBox';

import { Default as HeroBoxImageDefault } from './heroBoxImage/heroBoxImage.stories';
import { Default as HeroBoxTrailerDefault } from './heroBoxTrailer/heroBoxTrailer.stories';
import { HeroBoxImageProps } from './heroBoxImage';

export default {
  title: 'Molecules/HeroBox',
  component: HeroBox,
  parameters: {
    layout: 'fullscreen',
  },
} as Meta;

const Template: Story<HeroBoxProps> = (args) => <HeroBox {...args}>Content</HeroBox>;

export const Default = Template.bind({});
Default.args = {
  image: HeroBoxImageDefault.args as HeroBoxImageProps,
  ...HeroBoxTrailerDefault.args,
};

export const Mobile = Template.bind({
  ...Default.args,
});
Mobile.args = { ...Default.args };
Mobile.parameters = {
  viewport: {
    defaultViewport: 'iphonex',
  },
};
