import React from 'react';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import { HeroBoxTrailer, HeroBoxTrailerProps } from './HeroBoxTrailer';
import { useMediaQueries } from '../../../particules';

jest.mock('../../../particules/hooks/useMediaQueries');

const mockUseMediaQueries = (useMediaQueries as unknown) as jest.Mock;

const renderer = (
  props?: Partial<HeroBoxTrailerProps>,
): [RenderResult, Record<string, jest.Mock>] => {
  const mocks = {
    onMore: jest.fn(),
    onAdd: jest.fn(),
  };

  const mergeProps = {
    watchLink: '/watch/123',
    title: 'Title',
    synopsis: 'synopsis',
    ...mocks,
    ...props,
  };

  return [render(<HeroBoxTrailer {...mergeProps} />), mocks];
};

describe('molecules > HeroBoxTrailer', () => {
  describe('snapshot', () => {
    it('default', () => {
      const [{ asFragment }] = renderer();

      expect(asFragment()).toMatchSnapshot();
    });

    it('without synopsis', () => {
      const [{ asFragment }] = renderer({ synopsis: undefined });

      expect(asFragment()).toMatchSnapshot();
    });
  });

  describe('call to action', () => {
    it('add button', () => {
      const [{ getByRole }, { onAdd }] = renderer();

      fireEvent.click(getByRole('button', { name: /Ma list/ }));

      expect(onAdd).toHaveBeenCalledTimes(1);
    });

    it('more button (mobile)', () => {
      mockUseMediaQueries.mockImplementationOnce(() => true);
      const [{ getByRole }, { onMore }] = renderer();

      fireEvent.click(getByRole('button', { name: /Infos/ }));

      expect(onMore).toHaveBeenCalledTimes(1);
    });

    it('more button', () => {
      mockUseMediaQueries.mockImplementationOnce(() => false);
      const [{ getByRole }, { onMore }] = renderer();

      fireEvent.click(getByRole('button', { name: /Plus d'infos/ }));

      expect(onMore).toHaveBeenCalledTimes(1);
    });

    it('watch anchor', () => {
      const [{ getByRole }] = renderer();

      const link = getByRole('link', { name: /Lecture/ });
      expect(link).toHaveAttribute('href', '/watch/123');
    });
  });
});
