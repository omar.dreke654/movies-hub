/** @jsx jsx */
import { jsx } from '@emotion/core';
import { Story, Meta } from '@storybook/react/types-6-0';
import { mq } from '../../../particules';
import { HeroBoxTrailer, HeroBoxTrailerProps } from './HeroBoxTrailer';

export default {
  title: 'Molecules/HeroBox/Trailer',
  component: HeroBoxTrailer,
  parameters: {
    layout: 'fullscreen',
  },
} as Meta;

const Template: Story<HeroBoxTrailerProps> = (args) => (
  <div css={{ position: 'relative', height: '30vh', [mq.small]: { height: '100vh' } }}>
    <HeroBoxTrailer {...args}>Content</HeroBoxTrailer>
  </div>
);

export const Default = Template.bind({});
Default.args = {
  title: 'Toy Story',
  synopsis:
    'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
};

export const Mobile = Template.bind({});
Mobile.args = {
  ...Default.args,
};
Mobile.parameters = {
  viewport: {
    defaultViewport: 'iphonex',
  },
};
