/** @jsx jsx */
import { jsx } from '@emotion/core';
import { Story, Meta } from '@storybook/react/types-6-0';
import { HeroBoxImage, HeroBoxImageProps } from './HeroBoxImage';

export default {
  title: 'Molecules/HeroBox/Image',
  component: HeroBoxImage,
  parameters: {
    layout: 'fullscreen',
  },
} as Meta;

const Template: Story<HeroBoxImageProps> = (args) => (
  <div
    css={{
      height: '56.25vh',
      width: '100%',
      position: 'relative',
    }}
  >
    <HeroBoxImage {...args}>Content</HeroBoxImage>
  </div>
);

export const Default = Template.bind({});
Default.args = {
  srcSets: [
    {
      source: 'https://image.tmdb.org/t/p/original/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 1199px)',
    },
    {
      source: 'https://image.tmdb.org/t/p/w1280/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 779px)',
    },
    {
      source: 'https://image.tmdb.org/t/p/w780/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 500px)',
    },
    {
      // source: 'https://image.tmdb.org/t/p/w500/7G9915LfUQ2lVfwMEEhDsn3kT4B.jpg',
      source: 'https://image.tmdb.org/t/p/w500/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(max-width: 499px)',
    },
  ],
  source: 'https://image.tmdb.org/t/p/w1280/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
  alt: 'Buzzzz',
};

export const Mobile = Template.bind({});
Mobile.args = { ...Default.args };
Mobile.parameters = {
  viewport: {
    defaultViewport: 'iphonex',
  },
};
