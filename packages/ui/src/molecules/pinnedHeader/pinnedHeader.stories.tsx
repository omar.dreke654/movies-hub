/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { PinnedHeader, PinnedHeaderProps } from './PinnedHeader';
import { MainNavigation } from '../mainNavigation';
import { HeaderBrand } from '../headerBrand';

export default {
  title: 'Molecules/PinnedHeader',
  component: PinnedHeader,
  parameters: {
    layout: 'fullscreen',
  },
  decorators: [
    (StoryFn): React.ReactElement => (
      <div style={{ height: '2000px' }}>
        <StoryFn />
      </div>
    ),
  ],
} as Meta;

const Template: Story<PinnedHeaderProps> = (args) => (
  <PinnedHeader {...args}>Lorem Ipsum ...</PinnedHeader>
);

export const Default = Template.bind({});
Default.args = {};

const WithMainNavigationTpl: Story<PinnedHeaderProps> = (args) => {
  return (
    <PinnedHeader {...args}>
      <HeaderBrand />

      <MainNavigation currentPath="/movies">
        <MainNavigation.Link href="/browse">Home</MainNavigation.Link>
        <MainNavigation.Link href="/movies">Movies</MainNavigation.Link>
        <MainNavigation.Link href="/tv-shows">TV Shows</MainNavigation.Link>
        <MainNavigation.Link href="/watchlist">Watchlist</MainNavigation.Link>
        <MainNavigation.Link href="/genres">Genres</MainNavigation.Link>
      </MainNavigation>
    </PinnedHeader>
  );
};

export const WithMainNavigation = WithMainNavigationTpl.bind({});
WithMainNavigation.args = {};

export const WithMainNavigationOnMobile = WithMainNavigationTpl.bind({});
WithMainNavigationOnMobile.args = {};
WithMainNavigationOnMobile.parameters = {
  viewport: {
    defaultViewport: 'iphonex',
  },
};
