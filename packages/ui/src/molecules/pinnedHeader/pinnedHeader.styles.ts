import { css } from '@emotion/core';
import { mq } from '../../particules';

export const container = css`
  top: 0;
  left: 0;
  right: 0;
  background-color: var(--background-color);
  transition: background-color 0.4s;
  position: fixed;

  z-index: 1;

  ${mq.large} {
    background-color: transparent;
  }
`;

export const leaveContainer = css`
  background-color: var(--background-color);
  ${mq.large} {
    background-color: var(--background-color);
  }
`;
