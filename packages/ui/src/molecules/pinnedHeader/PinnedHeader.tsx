/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import { useInView } from 'react-intersection-observer';

import { Header } from '../../atoms';

import * as styles from './pinnedHeader.styles';

export type PinnedHeaderProps = React.HTMLProps<HTMLDivElement> & React.PropsWithChildren<unknown>;

export const PinnedHeader = ({ children, ...props }: PinnedHeaderProps): React.ReactElement => {
  const { ref, inView } = useInView({ threshold: 1 });

  return (
    <div ref={ref} {...props}>
      <div
        css={[styles.container, !inView && styles.leaveContainer]}
        data-testid="pinned-header-wrapper"
      >
        <Header>{children}</Header>
      </div>
    </div>
  );
};
