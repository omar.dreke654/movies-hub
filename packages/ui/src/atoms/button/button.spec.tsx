import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import { Button } from './Button';
import { ButtonVariantColors, ButtonVariants } from './button.styles';

describe('atoms > Button', () => {
  describe('snapshot', () => {
    const variantColors: ButtonVariantColors[] = ['primary', 'secondary'];
    const variants: ButtonVariants[] = ['text', 'contained', 'underline'];

    it('default', () => {
      const { asFragment } = render(
        <Button>
          <span>content</span>
        </Button>,
      );

      expect(asFragment()).toMatchSnapshot();
    });

    variantColors.forEach((color) => {
      variants.forEach((variant) => {
        it(`${color}-${variant}`, () => {
          const { asFragment } = render(
            <Button variant={variant} color={color}>
              <span>content</span>
            </Button>,
          );

          expect(asFragment()).toMatchSnapshot();
        });
      });
    });
  });

  describe('direction', () => {
    it('Should display column mode', () => {
      const { getByRole } = render(
        <Button direction="column">
          <span>content</span>
        </Button>,
      );

      expect(getByRole('button')).toHaveStyle('flex-direction: column');
    });

    it('Should display row mode', () => {
      const { getByRole } = render(
        <Button direction="row">
          <span>content</span>
        </Button>,
      );

      expect(getByRole('button')).toHaveStyle('flex-direction: row');
    });
  });

  describe('startIcon', () => {
    it('Should not display startIcon', () => {
      const { getByRole } = render(
        <Button direction="column">
          <span>content</span>
        </Button>,
      );

      expect(() => getByRole('svg')).toThrow();
    });

    it('Should display startIcon', () => {
      const { getByTitle } = render(
        <Button startIcon="play">
          <span>content</span>
        </Button>,
      );

      expect(getByTitle('play')).toBeInTheDocument();
    });
  });

  it('Should trigger onClick method', () => {
    const onClick = jest.fn();

    const { getByRole } = render(
      <Button onClick={onClick}>
        <span>content</span>
      </Button>,
    );

    fireEvent.click(getByRole('button'));

    expect(onClick).toHaveBeenCalledTimes(1);
  });
});
