import { css, SerializedStyles } from '@emotion/core';

export type ButtonDirections = 'row' | 'column';
export const base = (direction: ButtonDirections): SerializedStyles => css`
  display: flex;
  flex-direction: ${direction};
  justify-content: center;
  align-items: center;
  padding: 0.6em;
  padding-left: 1.4em;
  padding-right: 1.8em;
  border: 0;
  border-radius: 4px;
  appearance: none;
  background: transparent;
  outline: none;

  white-space: nowrap;
  word-break: break-word;
  will-change: background-color, color;
  user-select: none;
  cursor: pointer;

  > div {
    line-height: 0;
  }

  > span {
    display: block;
    font-weight: 700;
    font-size: 1.1em;
    line-height: 1.5em;
  }
`;

export const spacer = css`
  width: 0.6em;
`;

export const startIcon = css`
  font-size: 1.6em;
`;

// -------- VARIANTS
export type ButtonVariantColors = 'primary' | 'secondary';
export type ButtonVariants = 'text' | 'contained' | 'underline';

export const variants: Record<ButtonVariantColors, Record<ButtonVariants, SerializedStyles>> = {
  primary: {
    text: css`
      color: #000000;
      :hover,
      :active {
        color: rgba(0, 0, 0, 0.7);
      }
    `,
    contained: css`
      background-color: #ffffff;
      color: #000000;

      :hover {
        background-color: rgba(255, 255, 255, 0.75);
      }
    `,
    underline: css`
      color: #000000;
      border: 1px solid #000000;

      :hover,
      :active {
        background-color: rgba(0, 0, 0, 0.4);
      }
    `,
  },
  secondary: {
    text: css`
      color: #ffffff;
      :hover,
      :active {
        color: rgba(255, 255, 255, 0.7);
      }
    `,
    contained: css`
      background-color: rgb(109, 109, 110, 0.7);
      color: #ffffff;

      :hover {
        background-color: rgb(109, 109, 110, 0.4);
      }
    `,
    underline: css`
      color: #ffffff;
      border: 1px solid #ffffff;

      :hover,
      :active {
        background-color: rgba(255, 255, 255, 0.4);
      }
    `,
  },
};
