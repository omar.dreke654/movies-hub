/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import { IconType } from 'react-icons/lib';
import { IoMdPlay, IoMdAdd, IoMdInformationCircleOutline } from 'react-icons/io';
import * as styles from './button.styles';
import { MergeProps } from '../../types';

type StartIcon = 'play' | 'info' | 'add';

const ICONS: Record<StartIcon, IconType> = {
  play: IoMdPlay,
  add: IoMdAdd,
  info: IoMdInformationCircleOutline,
};

export type ButtonProps = MergeProps<
  React.PropsWithChildren<{
    variant?: styles.ButtonVariants;
    color?: styles.ButtonVariantColors;
    startIcon?: StartIcon;
    direction?: styles.ButtonDirections;
  }>,
  React.HTMLProps<HTMLButtonElement>
>;

export const Button = ({
  children,
  startIcon,
  variant = 'contained',
  color = 'primary',
  direction = 'row',
  ...buttonProps
}: ButtonProps): React.ReactElement => {
  const Icon = startIcon ? ICONS[startIcon] : null;

  return (
    <button
      css={[styles.base(direction), styles.variants[color][variant]]}
      {...buttonProps}
      type="button"
    >
      {startIcon && (
        <React.Fragment>
          <div css={styles.startIcon}>{Icon && <Icon title={startIcon} />}</div>
          <div css={styles.spacer} />
        </React.Fragment>
      )}
      <span>{children}</span>
    </button>
  );
};
