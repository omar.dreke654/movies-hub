import { css } from '@emotion/core';
import { mq } from '../../particules';

export const container = css`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  z-index: 2;

  padding: 0 4%;

  min-height: var(--header-height-sm);

  transition: background-color 0.6s ease;
  background-color: transparent;
  background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0.7) 10%, rgba(0, 0, 0, 0));

  &.solid {
    background-color: var(--background-color);
  }

  ${mq.large} {
    min-height: var(--header-height);
  }
`;
