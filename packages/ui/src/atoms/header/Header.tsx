/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import * as styles from './header.styles';

export type HeaderProps = React.HTMLProps<HTMLDivElement> & React.PropsWithChildren<unknown>;

export const Header = ({ children }: HeaderProps): React.ReactElement => {
  return (
    <div css={styles.container} role="navigation">
      {children}
    </div>
  );
};
