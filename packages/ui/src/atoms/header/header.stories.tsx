import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { Header, HeaderProps } from './Header';

export default {
  title: 'Atoms/Header',
  component: Header,
} as Meta;

const Template: Story<HeaderProps> = (args) => (
  <Header {...args}>
    <div>Content 1</div>
    <div>Content 2</div>
  </Header>
);

export const Default = Template.bind({});
Default.args = {};
