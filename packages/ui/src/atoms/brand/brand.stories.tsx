import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { Brand, BrandProps } from './Brand';

export default {
  title: 'Atoms/Brand',
  component: Brand,
} as Meta;

const Template: Story<BrandProps> = () => <Brand />;

export const Default = Template.bind({});
Default.args = {};
