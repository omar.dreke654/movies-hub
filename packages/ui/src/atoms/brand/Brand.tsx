/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import * as styles from './brand.styles';

export type BrandProps = React.HTMLProps<HTMLSpanElement> & {
  variant?: 'full' | 'short';
};

export const Brand = ({ variant = 'full', ...htmlProps }: BrandProps): React.ReactElement => {
  if (variant === 'short') {
    return (
      <span css={styles.container} {...htmlProps}>
        M<span>H</span>
      </span>
    );
  }

  return (
    <span css={styles.container} {...htmlProps}>
      MOVIES<span>HUB</span>
    </span>
  );
};
