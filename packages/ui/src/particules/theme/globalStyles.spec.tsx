import React from 'react';
import { render } from '@testing-library/react';
import { GlobalStyles, styles } from './GlobalStyles';

test('renders global styles', () => {
  const { container } = render(<GlobalStyles />);
  expect(container).toMatchSnapshot();
  expect(styles).toMatchSnapshot();
});
