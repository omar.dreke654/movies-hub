import { renderHook } from '@testing-library/react-hooks';
import { mockMatchMediaMatches } from '../../tests/mockMatchMediaMatches';
import { useMediaQueries } from './useMediaQueries';

describe('When use useMediaQueries', () => {
  it('Should return default value', () => {
    const { result } = renderHook(() => useMediaQueries([], [], true));

    expect(result.current).toBe(true);
  });

  it("Should return '2'", () => {
    mockMatchMediaMatches.mockImplementation((query) => query === 'query2');
    const { result } = renderHook(() => useMediaQueries(['query1', 'query2'], ['1', '2'], '0'));

    expect(result.current).toBe('2');
  });
});
