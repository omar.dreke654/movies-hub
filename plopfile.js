const fs = require('fs');
const prettier = require('prettier');
const glob = require('glob');
const path = require('path');
const { camelCase, pascalCase } = require('change-case');

const TEMPLATE_PATH = path.resolve(`${__dirname}/plop`);
const OUTPUT_TARGET = path.resolve(`${__dirname}/packages/ui/src`);

const format = (filePath) =>
  prettier
    .getFileInfo(filePath)
    .then(({ inferredParser: parser }) =>
      prettier.resolveConfig(__dirname).then((options) => ({
        ...options,
        parser,
        data: fs.readFileSync(filePath, 'utf-8'),
      })),
    )
    .then(({ data, ...options }) =>
      fs.writeFileSync(filePath, prettier.format(data, options), 'utf-8'),
    )
    .then(() => `Formatted ${filePath}`);

function findFiles(target) {
  const files = glob.sync(`${target}/**/*`, { nodir: true });

  return files;
}

module.exports = function config(plop) {
  plop.setHelper('camelCase', (text) => camelCase(text));
  plop.setHelper('pascalCase', (text) => pascalCase(text));

  plop.setGenerator('ui', {
    description: 'create ui components',
    prompts: [
      {
        type: 'list',
        choices: [
          { name: 'atom', value: 'atoms' },
          { name: 'molecule', value: 'molecules' },
          { name: 'organism', value: 'organisms' },
        ],
        name: 'type',
        message: 'Type of atomic components',
      },
      {
        type: 'input',
        name: 'name',
        message: 'Name of your Component',
      },
    ],
    actions: ({ name, type }) => {
      const TARGET = `${OUTPUT_TARGET}/${type}/${camelCase(name)}`;

      const actions = [
        {
          type: 'add',
          path: `${TARGET}/${name}.tsx`,
          templateFile: `${TEMPLATE_PATH}/component.tsx.hbs`,
        },
        {
          type: 'add',
          path: `${TARGET}/${camelCase(name)}.styles.ts`,
          templateFile: `${TEMPLATE_PATH}/component.styles.ts.hbs`,
        },
        {
          type: 'add',
          path: `${TARGET}/${camelCase(name)}.spec.tsx`,
          templateFile: `${TEMPLATE_PATH}/component.spec.tsx.hbs`,
        },
        {
          type: 'add',
          path: `${TARGET}/${camelCase(name)}.stories.tsx`,
          templateFile: `${TEMPLATE_PATH}/component.stories.tsx.hbs`,
        },
        {
          type: 'add',
          path: `${TARGET}/index.ts`,
          templateFile: `${TEMPLATE_PATH}/index.ts.hbs`,
        },
      ];

      actions.push(() => {
        const files = findFiles(TARGET);

        files.forEach((file) => {
          actions.push(() => format(file));
        });

        return `Found ${files.length} to format`;
      });

      return actions;
    },
  });
};
