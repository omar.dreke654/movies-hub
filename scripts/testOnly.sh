#!/usr/bin/env sh
set -e

packageName="$1"
shift 1

yarn test --watchAll --projects "packages/${packageName}" "$@"
